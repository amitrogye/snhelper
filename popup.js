// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

let newWindow = document.getElementById('NewWindowMenuItem');
let scriptInclude = document.getElementById('ScriptIncludeMenuItem');
let businessRule = document.getElementById('businessRuleMenuItem');
let logTail = document.getElementById('logTailMenuItem');
let backgroundScript = document.getElementById('backgroundScriptMenuItem');
let tableConfigAll = document.getElementById('tableConfigAllMenuItem');
chrome.storage.sync.get('color', function(data) {
    //changeColor.style.backgroundColor = data.color;
    //changeColor.setAttribute('value', data.color);
});
scriptInclude.onclick = function(element) {
    chrome.tabs.executeScript(
        //tabs[0].id, 
        { code: 'window.frames["gsft_main"].location.href = "sys_script_include_list.do";' });
};
businessRule.onclick = function(element) {
    chrome.tabs.executeScript(
        //tabs[0].id, 
        { code: 'window.frames["gsft_main"].location.href = "sys_script_list.do";' }
    );
};
newWindow.onclick = function(element) {
    chrome.tabs.executeScript(
        //tabs[0].id, 
        { code: 'window.open(window.frames[\'gsft_main\'].location.href);collapseMenu();' }
    );
};
logTail.onclick = function(element) {
    chrome.tabs.executeScript(
        //tabs[0].id, 
        { code: 'window.frames["gsft_main"].location.href = "channel.do?sysparm_channel=logtail";' }
    );
};
backgroundScript.onclick = function(element) {
    chrome.tabs.executeScript(
        //tabs[0].id, 
        { code: 'window.frames["gsft_main"].location.href = "sys.scripts.do";' }
    );
};
tableConfigAll.onclick = function(element) {
    chrome.tabs.executeScript(
        { code: 'window.frames["gsft_main"].location.href = "sys.scripts.do";' }
    );
};